/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 4;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int horizpadbar        = 0;        /* horizontal padding for statusbar */
static const int vertpadbar         = 5;        /* vertical padding for statusbar */
static const char *fonts[]          = { "mononoki:size=10" };
static const char col_gray1[]       = "#283033";
static const char col_gray2[]       = "#181D1F"; /* border color unfocused windows */
static const char col_gray3[]       = "#BAD4D6";
static const char col_gray4[]       = "#DDFCFE";
static const char col_cyan[]        = "#FF5E00"; /* border color focused windows and tags */
static const unsigned int baralpha = 0xbe;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3] =
  {
   /*               fg         bg         border   */
   [SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
   [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
  };
static const unsigned int alphas[][3] =
  {
   /*               fg      bg        border     */
   [SchemeNorm] = { OPAQUE, baralpha, borderalpha },
   [SchemeSel]  = { OPAQUE, baralpha, borderalpha },
  };

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] =
  {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class      instance    title       tags mask     isfloating   monitor */
    { "Gimp",     NULL,       NULL,       0,            1,           -1 },
    { "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
    { "pl.skmedix.bootstrap.Bootstrap", NULL, NULL, 0,  1,           -1 }, /* SK Launcher (minecraft) */
    { "Pavucontrol", NULL,    NULL,       0,            1,           -1 },
  };

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] =
  {
    /* symbol     arrange function */
    { "[T]",      tile },    /* first entry is default */
    { ">F>",      NULL },    /* no layout function means floating behavior */
    { "[M]",      monocle },
    { "|M|",      centeredmaster },
    { ">M>",      centeredfloatingmaster },
    { NULL,       NULL },
  };

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char term[] = "alacritty";
static const char *termcmd[] = { term, "-e", "tmux", "-u", NULL };
static const char *systemmonitorcmd[] = { term, "-e", "htop", NULL };
static const char *filemanagercmd[] = { term, "-e", "ranger", NULL };
static const char *irssicmd[] = { term, "-e", "irssi", NULL };
static const char *browsercmd[] = { "firefox-nightly", NULL };
static const char *emacscmd[] = { "emacs", NULL };
static const char *codecmd[] = { "code-insiders", NULL };

/* scratchpad(s) */
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { term, "-t", scratchpadname, "-d", "240", "60", "-e", "tmux", "-u", NULL };

#include <X11/XF86keysym.h>
static Key keys[] = {
    /* modifier                     key        function        argument */

    /* dwm */
    { MODKEY|ShiftMask,             XK_l,      quit,           { 0 } },

    /* layout */
    { ALTKEY,                       XK_1,      setlayout,      { .v = &layouts[0] } },
    { ALTKEY,                       XK_2,      setlayout,      { .v = &layouts[1] } },
    { ALTKEY,                       XK_3,      setlayout,      { .v = &layouts[2] } },
    { ALTKEY,                       XK_4,      setlayout,      { .v = &layouts[3] } },
    { ALTKEY,                       XK_5,      setlayout,      { .v = &layouts[4] } },
    { ALTKEY,                       XK_i,      setlayout,      { 0 } },
    { ALTKEY,	                    XK_j,      cyclelayout,    { .i = -1 } },
    { ALTKEY,                       XK_k,      cyclelayout,    { .i = +1 } },

    { MODKEY,                       XK_Tab,    focusstack,     { .i = +1 } },
    { MODKEY|ShiftMask,             XK_Tab,    focusstack,     { .i = -1 } },
    { ALTKEY|ShiftMask,             XK_a,      focusstack,     { .i = +1 } },
    { ALTKEY|ShiftMask,             XK_d,      focusstack,     { .i = -1 } },
    { ALTKEY,                       XK_w,      incnmaster,     { .i = +1 } },
    { ALTKEY,                       XK_s,      incnmaster,     { .i = -1 } },
    { ALTKEY,                       XK_a,      setmfact,       { .f = -0.05 } },
    { ALTKEY,                       XK_d,      setmfact,       { .f = +0.05 } },

    { MODKEY,                       XK_j,      setgaps,        { .i = -1 } },
    { MODKEY,                       XK_k,      setgaps,        { .i = +1 } },

    /* window */
    { MODKEY|ShiftMask,             XK_q,      killclient,     { 0 } },
    { MODKEY|ShiftMask,             XK_f,      togglefloating, { 0 } },
    { ALTKEY|ShiftMask,             XK_w,      zoom,           { 0 } },

    /* application */
    { MODKEY,                       XK_Return, spawn,          { .v = termcmd } },
    { MODKEY|ControlMask,           XK_a,      spawn,          { .v = termcmd } },
    { MODKEY|ControlMask,           XK_z,      spawn,          { .v = browsercmd } },
    { MODKEY|ControlMask,           XK_x,      spawn,          { .v = emacscmd } },
    { MODKEY|ControlMask,           XK_c,      spawn,          { .v = codecmd } },
    { MODKEY|ControlMask,           XK_k,      spawn,          { .v = systemmonitorcmd } },
    { MODKEY|ControlMask,           XK_d,      spawn,          { .v = filemanagercmd } },
    { MODKEY|ControlMask,           XK_i,      spawn,          { .v = irssicmd } },

    /* dropdown */
    { MODKEY|ShiftMask,             XK_a,      togglescratch,  { .v = scratchpadcmd } },

    /* run menu */
    { MODKEY,                       XK_r,      spawn,          { .v = dmenucmd } },

    /* sound */
    { 0, XF86XK_AudioMute,		                 spawn,		       SHCMD("amixer -q set Master toggle")   },
    { 0, XF86XK_AudioRaiseVolume,	             spawn,		       SHCMD("amixer sset Master 5%+ unmute") },
    { 0, XF86XK_AudioLowerVolume,	             spawn,		       SHCMD("amixer sset Master 5%- unmute") },

    /* tag */
    TAGKEYS(                        XK_1,                       0 ),
    TAGKEYS(                        XK_2,                       1 ),
    TAGKEYS(                        XK_3,                       2 ),
    TAGKEYS(                        XK_4,                       3 ),
    TAGKEYS(                        XK_5,                       4 ),
    TAGKEYS(                        XK_6,                       5 ),
    TAGKEYS(                        XK_7,                       6 ),
    TAGKEYS(                        XK_8,                       7 ),
    TAGKEYS(                        XK_9,                       8 ),
    { MODKEY,                       XK_0,      view,           { .ui = ~0 } },
    { MODKEY|ShiftMask,             XK_0,      tag,            { .ui = ~0 } },

    // { ALTKEY,                       XK_comma,  focusmon,       {.i = -1 } },
    // { ALTKEY,                       XK_period, focusmon,       {.i = +1 } },
    // { ALTKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
    // { ALTKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

